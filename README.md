I can't believe Instagram doesn't let you navigate with HJKL
============================================================
This is a Tampermonkey script.

Usage
-----
j/k - Up or down in your timeline  
h/l - Left or right on articles with a carousel  
f - fav current article  
0 - go to top article  
/ - focus search bar  
esc - cancel search  
space - play or pause video  

Known Issues
------------
- This shit sucks

To Do
-----
- Add some more shortcuts:
    - new article (n)
    - reply (r)
    - expand article caption (return)
- set current article through click/interaction
