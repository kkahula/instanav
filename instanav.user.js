// ==UserScript==
// @name         instanav
// @namespace    https://gitlab.com/kkahula
// @version      0.1.2
// @description  sensible keyboard navigation for instagram
// @author       kkahula
// @match        https://*.instagram.com/*
// @grant        none
// @run-at document-end
// ==/UserScript==

(function () {
  /* global MutationObserver */
  'use strict'

  let currentArticle
  let topArticle
  let textInputTags = ['input', 'textarea']
  let searchField
  let articleObserver

  window.addEventListener('load', function () {
    let mainElement = document.querySelector('main')
    let observerConfig = { attributes: true, childList: true, subtree: true }
    let letterhead = document.querySelector('a[href="/"]')

    searchField = document.querySelector('input[type=text][placeholder=Search]')
    articleObserver = new MutationObserver(newArticlesLoaded)

    articleObserver.observe(mainElement, observerConfig)
    window.addEventListener('keydown', doHotkey)
    window.addEventListener('popstate', routeChanged)
    letterhead.addEventListener('click', clickLetterhead)
  })

  let routeChanged = function (event) {
    console.log('route')
  }

  let clickLetterhead = function (event) {
    console.log('letterhead')
  }

  let newArticlesLoaded = function (mutationsList, observer) {
    mapArticles()
    articleObserver.disconnect()
  }

  let mapArticles = function () {
    let articles = document.getElementsByTagName('article')
    if (articles.length) {
      currentArticle = topArticle = articles[0]
      setHighlighted(currentArticle)
    }
  }

  let getSearchClearButton = function () {
    return document.querySelector('div.coreSpriteSearchClear')
  }

  let cancelSearch = function () {
    if (document.activeElement && getSearchClearButton()) {
      document.activeElement.blur()
      getSearchClearButton().click()
    }
  }

  let setHighlighted = function (elem) {
    elem.style.outlineColor = 'rgb(77, 144, 254)'
    elem.style.outlineWidth = '5px'
    elem.style.outlineStyle = 'auto'
  }

  let unsetHighlighted = function (elem) {
    elem.style.outlineStyle = 'none'
  }

  let goToArticle = function (direction) {
    if (!document.body.contains(currentArticle)) {
      mapArticles()
      direction = 'top'
    }

    let newArticle
    if (direction === 'next') {
      newArticle = currentArticle.nextSibling
    } else if (direction === 'previous') {
      newArticle = currentArticle.previousSibling
    } else if (direction === 'top') {
      newArticle = topArticle
    }

    if (newArticle) {
      unsetHighlighted(currentArticle)
      setHighlighted(newArticle)
      currentArticle = newArticle
      currentArticle.scrollIntoView({ block: 'start', behavior: 'smooth' })
    }
  }

  let doHotkey = function (event) {
    if (event.key === 'Escape' && document.activeElement && getSearchClearButton()) { // back out of search
      cancelSearch()
    }

    if (textInputTags.includes(document.activeElement.tagName.toLowerCase())) {
      return
    }

    switch (event.key) {
      // timeline navigate
      case ('j'): // next
        goToArticle('next')
        break

      case ('k'): // previous
        goToArticle('previous')
        break

      // carousell navigate
      case ('h'): // left image
        if (currentArticle.querySelectorAll('div.coreSpriteLeftChevron').length) {
          currentArticle.querySelectorAll('div.coreSpriteLeftChevron')[0].parentElement.click()
        }
        break
      case ('l'): // right image
        if (currentArticle.querySelectorAll('div.coreSpriteRightChevron').length) {
          currentArticle.querySelectorAll('div.coreSpriteRightChevron')[0].parentElement.click()
        }
        break

      // do things
      case ('f'): // fav current
        // hopefully this is always the first button :)
        currentArticle.querySelectorAll('button span[class^="glyphsSpriteHeart_"]')[0].click()
        break

      case (' '): // play/pause current video
        event.preventDefault()
        if (currentArticle.querySelectorAll('a[role="button"]').length) {
          currentArticle.querySelectorAll('a[role="button"]')[0].click()
        }
        break

      // page navigate
      case ('/'): // search
        event.preventDefault()
        searchField.focus()
        break

      case ('0'): // go to top article
        goToArticle('top')
    }
  }
})()
